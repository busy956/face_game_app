import 'package:flutter/material.dart';

class PageFaceGame extends StatefulWidget {
  const PageFaceGame({
    super.key,
    required this.friendName

  });

  final String friendName;

  @override
  State<PageFaceGame> createState() => _PageFaceGameState();
}

class _PageFaceGameState extends State<PageFaceGame> {
  List<num> face = [1, 2];
  List<num> eyebrow = [1, 2, 3];
  List<num> eyes = [1, 2, 3];
  List<num> nose = [1, 2, 3];
  List<num> mouse = [1, 2, 3];
  num faceNumber = 0;
  num eyebrowNumber = 0;
  num eyesNumber = 0;
  num noseNumber = 0;
  num mouseNumber = 0;

  @override
  void initState() {
    super.initState();
    face.shuffle();
    eyebrow.shuffle();
    eyes.shuffle();
    nose.shuffle();
    mouse.shuffle();
    faceNumber = face[0];
    eyebrowNumber = eyebrow[0];
    eyesNumber = eyes[0];
    noseNumber = nose[0];
    mouseNumber = mouse[0];
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
            '얼굴 결과 공개'
        ),
      ),
      body: SingleChildScrollView(
        child: Center(
          child: Column(
            children: [
              const SizedBox(
                height: 100,
              ),
              Stack(
                children: [
                  Image.asset('assets/face$faceNumber.png'),
                  Image.asset('assets/eyebrow$eyebrowNumber.png'),
                  Image.asset('assets/eyes$eyesNumber.png'),
                  Image.asset('assets/nose$noseNumber.png'),
                  Image.asset('assets/mouse$mouseNumber.png'),
                ],
              ),
              const SizedBox(
                height: 50,
              ),
              Container(
                child: Text(widget.friendName),
              ),
              const SizedBox(
                height: 50,
              ),
              const SizedBox(
                height: 50,
              ),
              OutlinedButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  child: const Text('다시')
              ),
            ],
          ),
        ),
      ),
    );
  }
}
