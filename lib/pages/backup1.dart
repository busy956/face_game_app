import 'package:face_game_app/pages/page_face_game.dart';
import 'package:flutter/material.dart';

class PageIndex extends StatefulWidget {
  const PageIndex({super.key});

  @override
  State<PageIndex> createState() => _PageIndexState();
}

class _PageIndexState extends State<PageIndex> {
  String _friendName = '';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('친구 얼굴 그리기'),
      ),
      body: _buildBody(context),
    );
  }

  Widget _buildBody(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        child: Column(
          children: [
            const SizedBox(
              height: 200,
            ),
            TextField(
              decoration: InputDecoration(
                border: OutlineInputBorder(),
                labelText: 'name',
              ),
              onChanged: (text) {
                setState(() {
                  _friendName = text;
                });
              },
            ),
            const SizedBox(
              height: 150,
            ),
            OutlinedButton(
              onPressed: () {
                Navigator.of(context).push(
                  MaterialPageRoute(
                    builder: (BuildContext context) =>
                        PageFaceGame(friendName: _friendName),
                  ),
                );
              },
              child: const Text('시작'),
            ),
          ],
        ),
      ),
    );
  }
}
