import 'package:face_game_app/pages/page_face_game.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';

class PageIndex extends StatefulWidget {
  const PageIndex({super.key});

  @override
  State<PageIndex> createState() => _PageIndexState();
}

class _PageIndexState extends State<PageIndex> {
  final _formKey = GlobalKey<FormBuilderState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('친구 얼굴 그리기'),
      ),
      body: _buildBody(context),
    );
  }

  Widget _buildBody(BuildContext context) {
    return SingleChildScrollView(
      child: FormBuilder(
        key: _formKey,
        child: Column(
          children: [
            const SizedBox(height: 200,),
            FormBuilderTextField(
              name: 'friendName',
              decoration: const InputDecoration(labelText: '친구의 이름'),
              validator: FormBuilderValidators.compose([
                FormBuilderValidators.required(),
              ]),
            ),
            const SizedBox(height: 150,),
            MaterialButton(
              color: Theme.of(context).colorScheme.secondary,
              onPressed: () {
                if (_formKey.currentState?.saveAndValidate() ?? false) {
                 String friendName = _formKey.currentState!.fields['friendName']!.value;
                  Navigator.of(context).push(MaterialPageRoute(builder: (context) => PageFaceGame(friendName: friendName)));
                }
              },
              child: const Text('시작'),
            )
          ],
        ),
      ),
    );
  }
}
