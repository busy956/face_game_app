import 'package:face_game_app/components/component_appbar_normal.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';

class PageIndex extends StatefulWidget {
  const PageIndex({super.key});

  @override
  State<PageIndex> createState() => _PageIndexState();
}

class _PageIndexState extends State<PageIndex> {
  final _formKey = GlobalKey<FormBuilderState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          title:Text('너 얼굴')
      ),
      body: FormBuilder(
        key: _formKey,
        child: Column(
          children: [
            FormBuilderTextField(
              name: 'friendName',
              decoration: const InputDecoration(labelText: '친구이름'),
              validator: FormBuilderValidators.compose([
                FormBuilderValidators.required(),
              ]),
            ),
            MaterialButton(
              color: Theme.of(context).colorScheme.secondary,
              onPressed: () {
                //만약에.. 폼에 값이 있으면.. (값이 없으면 false가 리턴되어서 if문이 실행되지 않음)
              }
                if (_formKey.currentState?.saveAndValidate() ?? false) {
                  String friendName = _formKey.currentState!.fields['friendName']!.value;
                  Navigator.of(context).push(MaterialPageRoute(builder: (context) => PageFaceGame()))
                }
              },
              child: const Text('Login'),
            )
          ],
        ),
      ),
    );
  }
}
