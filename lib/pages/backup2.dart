import 'dart:math';

import 'package:flutter/material.dart';

class PageFaceGame extends StatefulWidget {
  const PageFaceGame({super.key, required this.friendName});

  final String friendName;

  @override
  State<PageFaceGame> createState() => _PageFaceGameState();
}

class _PageFaceGameState extends State<PageFaceGame> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
            '얼굴 결과 공개'
        ),
      ),
      body: SingleChildScrollView(
        child: Center(
          child: Column(
            children: [
              const SizedBox(
                height: 100,
              ),
              Stack(
                children: [
                  Image.asset('assets/face1.png'),
                  Image.asset('assets/eyebrow1.png'),
                  Image.asset('assets/eyes1.png'),
                  Image.asset('assets/nose1.png'),
                  Image.asset('assets/mouse1.png'),
                ],
              ),
              const SizedBox(
                height: 50,
              ),
              Container(
                child: Text(widget.friendName),
              ),
              const SizedBox(
                height: 50,
              ),
              OutlinedButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  child: const Text('다시'))
            ],
          ),
        ),
      ),
    );
  }
}
